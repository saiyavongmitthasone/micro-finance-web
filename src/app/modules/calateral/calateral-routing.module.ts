import { CalateralinformationComponent } from './submodules/calateralinformation/calateralinformation.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: 'calateral-information', component: CalateralinformationComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalateralRoutingModule { }
