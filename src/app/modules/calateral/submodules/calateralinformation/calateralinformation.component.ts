import { Component, OnInit, ViewChild } from '@angular/core';
import { InteractivityChecker } from '@angular/cdk/a11y';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

export interface calateralData{
  CalateralID: string;
  CalateralType: string;
  CalateralName: string;
  CalateralValue: string;
  MarketValue: string;
}
export interface CalateralAddress{
  Street: string;
  Village: string;
  City: string;
  Province: string;
  Picture: string;
}

@Component({
  selector: 'app-calateralinformation',
  templateUrl: './calateralinformation.component.html',
  styleUrls: ['./calateralinformation.component.css']
})
export class CalateralinformationComponent implements OnInit {
  displayCalateralColumns: string[] =['CalateralID','CalateralType','CalateralName','CalateralValue','MarketValue']
  displayCalateralAddressColumns: string[] = ['Street', 'Village', 'City', 'Province','Picture']

  CalateralData: MatTableDataSource<calateralData>
  CalateralAddressData: MatTableDataSource<CalateralAddress>

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

 
  constructor() { }

  ngOnInit() {
    this.CalateralData.paginator = this.paginator;
    this.CalateralData.sort = this.sort;
 
  }

}
