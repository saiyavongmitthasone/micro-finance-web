import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalateralRoutingModule } from './calateral-routing.module';
import { DemoMaterialModule } from 'src/app/shares/module/materialcomponents/materialcomponents.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CalateralinformationComponent } from './submodules/calateralinformation/calateralinformation.component';

@NgModule({
  imports: [
    CommonModule,
    CalateralRoutingModule,
    DemoMaterialModule,
    FlexLayoutModule,
  ],
  declarations: [CalateralinformationComponent]
})
export class CalateralModule { }
