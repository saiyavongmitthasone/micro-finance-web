import { FlexLayoutModule } from '@angular/flex-layout';
import { DemoMaterialModule } from './../../shares/module/materialcomponents/materialcomponents.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SystemRoutingModule } from './system-routing.module';
import { ProductsComponent } from './submodules/products/products.component';
import { PrincipalComponent } from './submodules/products/components/principal/principal.component';
import { InterestComponent } from './submodules/products/components/interest/interest.component';
import { ProductComponentComponent } from './submodules/products/components/product-component/product-component.component';
import { ProductEventComponent } from './submodules/products/components/product-event/product-event.component';
import { ProductConditionComponent } from './submodules/products/components/product-condition/product-condition.component';
import { ProductAccountingEntriesComponent } from './submodules/products/components/product-accounting-entries/product-accounting-entries.component';
import { ProductAccountingFormulaComponent } from './submodules/products/components/product-accounting-formula/product-accounting-formula.component';
import { ProductOptionsComponent } from './submodules/products/components/product-options/product-options.component';
import { ProductMainComponent } from './submodules/products/components/product-main/product-main.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserControlComponent } from './submodules/components/user-control/user-control.component';
import { CustomerGroupComponent } from './submodules/components/customer-group/customer-group.component';
import { BranchManagementComponent } from './submodules/branch-management/branch-management.component';
import { InterBranchSettingComponent } from './submodules/inter-branch-setting/inter-branch-setting.component';
import { AdjustOrPostingDataEntriesComponent } from './submodules/ັs/adjust-or-posting-data-entries/adjust-or-posting-data-entries.component';

@NgModule({
  imports: [
    CommonModule,
    SystemRoutingModule,
    DemoMaterialModule,
    FlexLayoutModule,
    BrowserAnimationsModule
  ],
  declarations: [
    ProductsComponent, 
    PrincipalComponent, 
    InterestComponent, 
    ProductComponentComponent, 
    ProductEventComponent, 
    ProductConditionComponent, 
    ProductAccountingEntriesComponent, 
    ProductAccountingFormulaComponent, 
    ProductOptionsComponent, 
    ProductMainComponent, 
    UserControlComponent, 
    CustomerGroupComponent, 
    BranchManagementComponent, InterBranchSettingComponent, AdjustOrPostingDataEntriesComponent],
  
})
export class SystemModule { }
