import { trigger, stagger, query, transition, style, animate } from '@angular/animations';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

export const branchSettingAnimation = trigger('branchSettingAnimation',[
  transition(':enter',[
    query('.box',style({
      opacity: 0
    })),
    query('.box', stagger(50, [
      style({
        transform: 'translateY(500px)'
      }),
      animate('1s cubic-bezier(.75,-0.48,.26,1.52)',
        style({
          transform: 'translateX(0px)',
          opacity: 1,
        })
      )
    ]))
  ])
])
export interface branchData {
  Street: string,
  Village: string,
  City: string,
  Province: string,
  Picture: string
}
@Component({
  selector: 'app-branch-management',
  templateUrl: './branch-management.component.html',
  styleUrls: ['./branch-management.component.css'],
  animations: [branchSettingAnimation],
  host: {
    '[@branchSettingAnimation]': ''
  }
})
export class BranchManagementComponent implements OnInit {
  sampleData = [
    {Street: 'xxxxx', Village: 'xxxxx', City: 'xxxxx', Province: 'xxxxx', Picture: 'xxxxx'},
    {Street: 'xxxxx', Village: 'xxxxx', City: 'xxxxx', Province: 'xxxxx', Picture: 'xxxxx'},
    {Street: 'xxxxx', Village: 'xxxxx', City: 'xxxxx', Province: 'xxxxx', Picture: 'xxxxx'},
    {Street: 'xxxxx', Village: 'xxxxx', City: 'xxxxx', Province: 'xxxxx', Picture: 'xxxxx'},
    {Street: 'xxxxx', Village: 'xxxxx', City: 'xxxxx', Province: 'xxxxx', Picture: 'xxxxx'},
    {Street: 'xxxxx', Village: 'xxxxx', City: 'xxxxx', Province: 'xxxxx', Picture: 'xxxxx'},
  ]
  displayedColumns: string[]=['Street', 'Village', 'City', 'Province', 'Picture']
  dataSource: MatTableDataSource<branchData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor() {
    this.dataSource = new MatTableDataSource(this.sampleData);
   }

  ngOnInit() {
   
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}