import { Component, OnInit } from '@angular/core';
import {MatTabsModule} from '@angular/material/tabs';
import { trigger, transition, group, query, style, animate } from '@angular/animations';
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],

})
export class ProductsComponent implements OnInit {


  accent = "#0680f9"
  primary = "#0680f9"
  constructor() { }

  ngOnInit() {
  }

  getDepth(outlet) {
    return outlet.activatedRouteData['depth'];
  }

}
