import { AdjustOrPostingDataEntriesComponent } from './submodules/ັs/adjust-or-posting-data-entries/adjust-or-posting-data-entries.component';
import { BranchManagementComponent } from './submodules/branch-management/branch-management.component';
import { CustomerGroupComponent } from './submodules/components/customer-group/customer-group.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProductAccountingFormulaComponent } from './submodules/products/components/product-accounting-formula/product-accounting-formula.component';
import { ProductEventComponent } from './submodules/products/components/product-event/product-event.component';
import { ProductConditionComponent } from './submodules/products/components/product-condition/product-condition.component';
import { ProductComponentComponent } from './submodules/products/components/product-component/product-component.component';
import { InterestComponent } from './submodules/products/components/interest/interest.component';
import { PrincipalComponent } from './submodules/products/components/principal/principal.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsComponent } from './submodules/products/products.component';
import { ProductMainComponent } from './submodules/products/components/product-main/product-main.component';
import { ProductAccountingEntriesComponent } from './submodules/products/components/product-accounting-entries/product-accounting-entries.component';
import { UserControlComponent } from './submodules/components/user-control/user-control.component';
import { InterBranchSettingComponent } from './submodules/inter-branch-setting/inter-branch-setting.component';

const routes: Routes = [
  {
    path: 'products', component: ProductsComponent, data: { depth: 1 }, children: [
      { path: '', component: ProductMainComponent, data: { depth: 2 } },
      { path: 'product-component', component: ProductComponentComponent, data: { depth: 2 } },
      { path: 'conditions', component: ProductConditionComponent, data: { depth: 2 } },
      { path: 'events', component: ProductEventComponent, data: { depth: 2 } },
      { path: 'accounting-entries', component: ProductAccountingEntriesComponent, data: { depth: 2 } },
      { path: 'formulas', component: ProductAccountingFormulaComponent, data: { depth: 2 } },
    ]
  },
  { path: 'user-maintenance', component: UserControlComponent},
  { path: 'principal', component: PrincipalComponent },
  { path: 'interest', component: InterestComponent },
  { path: 'customer-group', component: CustomerGroupComponent },
  { path: 'branch-management', component: BranchManagementComponent },
  { path: 'inter-branch-setting', component: InterBranchSettingComponent },
  { path: 'adjust-or-posting-data-entries', component: AdjustOrPostingDataEntriesComponent },


];


@NgModule({
  imports: [RouterModule.forChild(routes), BrowserAnimationsModule],
  exports: [RouterModule, BrowserAnimationsModule]
})
export class SystemRoutingModule { }
