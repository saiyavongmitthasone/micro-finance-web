import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomersRoutingModule } from './customers-routing.module';
import { CustomerProfileComponent } from './submodules/customer-profile/customer-profile.component';
import { DemoMaterialModule } from 'src/app/shares/module/materialcomponents/materialcomponents.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomerRegisterComponent } from './submodules/customer-register/customer-register.component';
import { CustomerInformationComponent } from './submodules/customer-information/customer-information.component';


@NgModule({
  imports: [
    CommonModule,
    CustomersRoutingModule,
    DemoMaterialModule,
    FlexLayoutModule,
    BrowserAnimationsModule
  ],
  declarations: [CustomerProfileComponent, CustomerRegisterComponent, CustomerInformationComponent]
})
export class CustomersModule { }
