import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { branchData } from 'src/app/modules/system/submodules/branch-management/branch-management.component';
export interface DocumentData {
  DocumentType: string,
  DocumentName: string,
  DateRecieve: string,
  Link: string,

}
@Component({
  selector: 'app-customer-register',
  templateUrl: './customer-register.component.html',
  styleUrls: ['./customer-register.component.css']
})
export class CustomerRegisterComponent implements OnInit {
  sampleData = [
    {DocumentType: 'xxxxx', DocumentName: 'xxxxx', DateRecieve: 'xxxxx', Link: 'xxxxx'},
    {DocumentType: 'xxxxx', DocumentName: 'xxxxx', DateRecieve: 'xxxxx', Link: 'xxxxx'},
    {DocumentType: 'xxxxx', DocumentName: 'xxxxx', DateRecieve: 'xxxxx', Link: 'xxxxx'},
    {DocumentType: 'xxxxx', DocumentName: 'xxxxx', DateRecieve: 'xxxxx', Link: 'xxxxx'},
    {DocumentType: 'xxxxx', DocumentName: 'xxxxx', DateRecieve: 'xxxxx', Link: 'xxxxx'},
    {DocumentType: 'xxxxx', DocumentName: 'xxxxx', DateRecieve: 'xxxxx', Link: 'xxxxx'},

  ]
  displayedColumns: string[]=['DocumentType', 'DocumentName', 'DateRecieve', 'Link']
  dataSource: MatTableDataSource<DocumentData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor() {
    this.dataSource = new MatTableDataSource(this.sampleData);
   }

  ngOnInit() {
   
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

}
