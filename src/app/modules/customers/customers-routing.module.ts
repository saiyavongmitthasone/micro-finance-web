import { CustomerInformationComponent } from './submodules/customer-information/customer-information.component';
import { CustomerRegisterComponent } from './submodules/customer-register/customer-register.component';
import { CustomerProfileComponent } from './submodules/customer-profile/customer-profile.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'customer-profile', component: CustomerProfileComponent},
  { path: 'customer-register', component: CustomerRegisterComponent},
  { path: 'customer-information', component: CustomerInformationComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomersRoutingModule { }
