
import { Component, Inject, HostListener } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent {
ngOnInit(): void {
  //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
  //Add 'implements OnInit' to the class.
 
  
}
  mainMenu = [
    {
      "menuName": "System",
      "subMenu": [
       {"subMenuName":  "Product", "routerLink": "/products"},
       {"subMenuName":  "Transaction Code", "routerLink": "/transaction-code"},
       {"subMenuName":  "GL Group Code", "routerLink": "/gl-group-code"},
       {"subMenuName":  "Branch Management", "routerLink": "/branch-management"},
       {"subMenuName":  "User Group", "routerLink": "/user-group"},
       {"subMenuName":  "User Maintenance", "routerLink": "/user-maintenance"},
      ],
      "icon": "fa fa-dashboard"
    },
    {
      "menuName": "Customer",
      "subMenu": [
        {"subMenuName":  "Customer Group", "routerLink": "/customer-group"},
        {"subMenuName":  "Customer Information", "routerLink": "/customer-information"},
        {"subMenuName":  "Customer Profile", "routerLink": "/customer-profile"},
        {"subMenuName":  "MIS", "routerLink": "/MIS"},
      ],
      "icon": "fa fa-users"
    },
    {
      "menuName": "Calateral",
      "subMenu": [
        {"subMenuName":  "Calateral Management", "routerLink": "/calateral-management"},
      ],
      "icon": "fa fa-suitcase"
    },
    {
      "menuName": "Loan Account",
      "subMenu":[
      {"subMenuName":  "Loan Account Management", "routerLink": "/loan-account-manangement"},
      {"subMenuName":  "Disbursment", "routerLink": "/disbursement"},
      {"subMenuName":  "Schedule", "routerLink": "/schedule"},
      {"subMenuName":  "Payment", "routerLink": "/payment"},
      {"subMenuName":  "Loan Officer Managment", "routerLink": "/loan-officer-management"},
      {"subMenuName":  "Loan Change Officer", "routerLink": "/loan-change-officer"},
      {"subMenuName":  "View Schedule", "routerLink": "/view-schedule"},
      ],
      "icon": "fa fa-money"
    },

    {
      "menuName": "Provision",
      "subMenu": [
        {"subMenuName":  "Manual Change A to B", "routerLink": "/manual-change-a-to-b"},
        {"subMenuName":  "Manual Change B to C", "routerLink": "/manual-change-b-to-c"},
        {"subMenuName":  "Manual Change C to D", "routerLink": "/manual-change-c-to-d"},
        {"subMenuName":  "Manual Change D to E", "routerLink": "/manual-change-c-to-e"},
        {"subMenuName":  "Manual Change Back Prov", "routerLink": "/manual-change-back-prov"},


      ],
      "icon": "fa fa-money"
    },
    {
      "menuName": "Loan Change Contract",
      "subMenu": [
        {"subMenuName":  "Change Schedule Principle", "routerLink": "/change-schedule-principle"},
        {"subMenuName":  "Change Schedule Interest", "routerLink": "/change-schedule-interest"}
      ],
      "icon": "fa fa-money"
    },
    {
      "menuName": "Report",
      "subMenu": [
        {"subMenuName":  "ລາຍງານການຄ້າງຊໍາລະ", "routerLink": "/ລາຍງານການຄ້າງຊໍາລະ"},
        {"subMenuName":  "ລາຍງານການຊໍາລະປະຈໍາວັນ", "routerLink": "/"},
        {"subMenuName":  "ລາຍງາຍເງິນກູ້ແຍກແຕ່ລະປະເພດ", "routerLink": "/"},
        {"subMenuName":  "ລາຍງານຄ້າງຊໍາລະ ດອກເບ້ຍ-ຕົ້ນທຶນ", "routerLink": "/"},
        {"subMenuName":  "ລາຍງານບັນຊີປິດແລະເປີດ", "routerLink": "/"},
        {"subMenuName":  "ລາຍງານພະນັກງານຮັບຜິດຊອບລູກຄ້າ", "routerLink": "/"}
      ],
      "icon": "fa fa-file-text-o"
    }






  ]
  yvalue

  @HostListener('window:scroll') dosomethind(){
    console.log(window.scrollY);
     this.yvalue = window.scrollY
  }


  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
    
  constructor(private breakpointObserver: BreakpointObserver,

    ) {}
  

  }

 