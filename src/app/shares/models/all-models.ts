export class AccountMaster {
    constructor(
        public ACCOUNT_NUMBER: string,
        public ACC_CCY: string,
        public ACCOUNT_NAME: string,
        public BRANCH_CODE: string,
        public CUSTOMER_NO: string,
        public PRODUCT_CODE: string,
        public PRODUCT_NAME: string,
        public PRODUCT_CATEGORY: string,
        public ACCOUNT_OPEN_DATE: string,
        public MATURITY_DATE: string,
        public AMOUNT_FINANCED: string,
        public PRIMARY_APPLICANT_ID: string,
        public PRIMARY_APPLICANT_NAME: string,
        public DISB_DATE: string,
        public AMOUNT_DISBURSED: string,
        public DISB_CCY: string,
        public STOP_ACCRUALS: string,
        public PROVISION_AUTO: string,
        public PROVISION_NAME: string,
        public PROVISION_DESC: string,
        public ACCOUNT_STATUS: string,
        public LATEST_EVT: string,
        public LATEST_INT_PAYMENT_: string,
        public LATEST_PRINCIPAL_PAYMENT_DT: string,
        public NEXT_INT_DUE_DATE: string,
        public NEXT_PRINCIPAL_DUE_DATE: string,
        public MAKER_ID: string,
        public MAKER_DT_STAMP: string,
        public CHECKER_ID: string,
        public CHECKER_DT_STAMP: string,
        public ALT_ACC_NO: string,
    ) { }
}
export class Customer {
    constructor(
        public CUSTOMER_NO: string,
        public CUSTOMER_TYPE: string,
        public CUSTOMER_CATEGORY: string,
        public COUNTRY: string,
        public NATIONALITY: string,
        public LANGUAGE: string,
        public CUSTOMER_PREFIX: string,
        public FIRST_NAME: string,
        public LAST_NAME: string,
        public DATE_OF_BIRTH: string,
        public BAN_OF_BIRTH: string,
        public DISTRICT_OF_BIRTH: string,
        public PROVINCE_OF_BIRTH: string,
        public MINOR: string,
        public SEX: string,
        public P_NATIONAL_ID: string,
        public P_NATIONAL_ISSUE_DATE: string,
        public P_NATIONAL_EXP_DATE: string,
        public PASSPORT_NO: string,
        public PASSPORT_ISSUE_DATE: string,
        public PASSPORT_EXP_DATE: string,
        public FAMILY_BOOK_NO: string,
        public FAMILY_BOOK_BAN: string,
        public FAMILY_BOOK_DISTRICT: string,
        public FAMILY_BOOK_PROVINCE: string,
        public CUR_BAN: string,
        public CUR_DISTRICT: string,
        public CUR_PROVINCE: string,
        public PHONE_NUMBER: string,
        public FAX: string,
        public E_MAIL: string,
        public BUSINESS_LICENT: string,
        public MIS: string,
        public RECORD_STAT: string,
        public AUTH_STAT: string,
        public MAKER_ID: string,
        public MAKER_DT_STAMP: string,
        public CHECKER_ID: string,
        public CHECKER_DT_STAMP: string,
        public OVERALL_LIMIT: string,
        public LIMIT_CCY: string,
        public DOCUMENT_PATH: string,
    ) { }
}
export class Schedule {
    constructor(
        public ID: string,
        public ACCOUNT_NUMBER: string,
        public CCY: string,
        public BRANCH_CODE: string,
        public COMPONENT_NAME: string,
        public PRODUCT_CODE: string,
        public PRODUCT_NAME: string,
        public FORMULA_NAME: string,
        public SCH_START_DATE: string,
        public NO_OF_SCHEDULES: string,
        public FREQUENCY: string,
        public UNIT: string,
        public SCH_END_DATE: string,
        public AMOUNT: string,
        public PAYMENT_MODE: string,
        public CR_GL: string,
        public PAYMENT_DETAILS: string,
        public FIRST_DUE_DATE: string,
        public EMI_AMOUNT: string,
        public RECORD_STAT: string,
        public REMARKS: string,
    ) { }
}
export class Schedule_Detail {
    constructor(
        public ID: string,
        public ORDER_NUMBER: string,
        public ACCOUNT_NUMBER: string,
        public CCY: string,
        public COMPONENT_NAME: string,
        public PRODUCT_CODE: string,
        public PRODUCT_NAME: string,
        public AMOUNT: string,
        public START_DATE: string,
        public END_DATE: string,
        public NUM_OF_DAYS: string,
        public DUE_DATE: string,
        public PAID_AMOUNT: string,
        public RECORD_STAT: string,
        public TRN_DATE: string,
        public TRN_REF_NO: string,
        public SCHEDULE_ID: string,
    ) { }
}
export class Component {
    constructor(
        public ID: string,
        public COMPONENT_NAME: string,
        public COMPONENT_DESC: string,
        public CREATE_DATE: string,
    ) { }
}

export class LoanEntry {
    constructor(
        public ID: string,
        public TRN_REF_NO: string,
        public ACCOUNT_NUMBER: string,
        public ACC_CCY: string,
        public TRN_DATE: string,
        public EVENT: string,
        public ACC_BRANCH: string,
        public DRCR: string,
        public TRN_CODE: string,
        public FCY_AMOUNT: string,
        public EXCH_RATE: string,
        public LCY_AMOUNT: string,
        public VALUE_DATE: string,
        public RELATED_ACC: string,
        public RELATED_CUSTOMER: string,
        public AUTH_STAT: string,
        public CUST_GL: string,
        public FINANCIAL_CYCLE: string,
        public PRRIOD_CODE: string,
        public BATCH_NO: string,
        public USER_ID: string,
        public AUTH_ID: string,
        public TRN_DATE_STAMP: string,
    ) { }
}
export class Payment {
    constructor(
        public ID: string,
        public SCHEDULE_DETAIL_ID: string,
        public ORDER_NUMBER: string,
        public ACCOUNT_NUMBER: string,
        public ACC_CCY: string,
        public COMPONENT_NAME: string,
        public TRN_AMOUNT: string,
        public TRN_CCY: string,
        public START_DATE: string,
        public END_DATE: string,
        public NUM_OF_DAYS: string,
        public DUE_DATE: string,
        public TRN_DATE: string,
        public TRN_REF_NO: string,
        public USER_ID: string,
        public AUTH_ID: string,
    ) { }
}
export class Disbursment {
    constructor(
        public ID: string,
        public ACCOUNT_NUMBER: string,
        public ACC_CCY: string,
        public ACC_BRANCH: string,
        public DISB_DATE: string,
        public DISB_AMOUNT: string,
        public USER_ID: string,
        public AUTH_ID: string,
        public TRN_REF_NO: string,
        public TRN_DT: string,
    ) { }
}
export class Product {
    constructor(
        public ID: string,
        public PRODUCT_CODE: string,
        public PRODUCT_NAME: string,
        public PRODUCT_CATEGORY: string,
        public PRODUCT_TYPE: string,
        public PRODUCT_DESC: string,
        public PRODUCT_UNIT: string,
        public PRODUCT_TERM: string,
        public INT_PAYMENT_UNIT: string,
        public INT_PAYMENT_FREQUENCY: string,
        public PRICIPAL_PAYMENT_UNIT: string,
        public PRICIPAL_PAYMENT_FREQUENCY: string,
        public FORMULA_CODE: string,
        public FORMULA_NAME: string,
        public CREATE_DATE: string,
        public RECORD_STAT: string,
        public USER_ID: string,
        public GL_CODE: string,
    ) { }
}
export class ProductType {
    constructor(
        public ID: string,
        public PRODUCT_TYPE_NAME: string,
        public PRODUCT_TYPE_DESC: string,
        public CREATE_DATE: string,
        public RECORD_STAT: string,
    ) { }
}
export class Formula {
    constructor(
        public ID: string,
        public FORMULA_CODE: string,
        public FORMULA_NAME: string,
        public FORMULA_DESC: string,
        public FORMULA_TYPE: string,
        public NUM_OF_DAY_IN_YEAR: string,
    ) { }
}
export class Category {
    constructor(
        public ID: string,
        public CATEGORY_NAME: string,
        public CATEGORY_DESC: string,
        public CREATE_DATE: string,
        public RECORD_STAT: string,
    ) { }
}

export class Acrued {
    constructor(
        public ID: string,
        public ACCOUNT_NUMBER: string,
        public ACC_CCY: string,
        public ACC_BRANCH: string,
        public ACRUED_DATE: string,
        public FORMULA_CODE: string,
        public FORMULA_NAME: string,
        public ACRUED_AMOUNT: string,
        public ACRUED_CCY: string,
        public ACQUIRE_ID: string,
        public ACQUIRE_DATE: string,
        public RECORD_STAT: string,
        public USER_ID: string,
    ) {

    }
}
export class Acquired {
    constructor(
        public ID: string,
        public ACCOUNT_NUMBER: string,
        public ACC_CCY: string,
        public ACC_BRANCH: string,
        public ACQUIRE_DATE: string,
        public ACQUIRE_AMOUNT: string,
        public ACQUIRE_CCY: string,
        public RECORD_STAT: string,
        public USER_ID: string,
        public TRN_REF_NO: string,
        public TRN_DT: string,
    ) {
    }
}
export class LoanPurpose {
    constructor(
        public ID: string,
        public PURPOSE_NAME: string,
        public PURPOSE_DESC: string,
        public CREATE_DATE: string,
        public RECORD_STAT: string,
    ) { }
}
export class MIS {
    constructor(
        public ID: string,
        public MIS_CODE: string,
        public MIS_NAME: string,
        public MIS_DESC: string,
        public CREATE_DATE: string,
        public RECORD_STAT: string,
    ) { }
}
export class Provision {
    constructor(
        public ID: string,
        public PROVISION_NAME: string,
        public PROVISION_DESC: string,
        public CREATE_DATE: string,
        public RECORD_STAT: string,
    ) {

    }
}
export class Event {
    constructor(public ID: string,
        public EVENT_NAME: string,
        public EVENT_DESC: string,
        public CREATE_DATE: string,
        public RECORD_STAT: string,
        public GL_CODE: string,
        public DRCR: string,
        public TRN_CODE: string, ) { }

}
export class TranCode {
    constructor(
        public ID: string,
        public TRN_CODE: string,
        public TRN_DESC: string,
        public CREATE_DATE: string,
        public RECORD_STAT: string
    ) { }
}
export class GL {
    constructor(
        public ID: string,
        public BRANCH_CODE: string,
        public GL_CODE: string,
        public GL_DESC: string,
        public DRCR: string,
        public CREATE_DATE: string,
        public RECORD_STAT: string,
    ) { }
}
export class Calateral {
    constructor(
        public ID: string,
        public CUSTOMER_NO: string,
        public CUSTOMER_NAME: string,
        public CALATERAL_TYPE: string,
        public APPLICANT_NO: string,
        public APPLICANT_NAME: string,
        public APPLICANT_BAN: string,
        public APPLICANT_DISTRICT: string,
        public APPLICANT_PROVINCE: string,
        public APPLICANT_DESC: string,
        public MAKET_AMOUNT: string,
        public MAKET_AMOUNT_CCY: string,
        public LIMIT_LCY_AMOUNT: string,
        public LIMIT_FCY_AMOUNT: string,
        public FCY_CCY: string,
        public USER_ID: string,
        public CREATE_DATE: string,
        public DOCUMENT_PATH: string,
        public GOOGLE_MAP_PATH: string,
        public REMARKS: string,
        public RECODR_STAT: string,
    ) { }
}
export class ChargePayment {
    constructor(public ID: string,
        public ACCOUNT_NUMBER: string,
        public ACC_CCY: string,
        public ACC_BRANCH: string,
        public COMPONENT_NAME: string,
        public CHARGE_DATE: string,
        public FORMULA_CODE: string,
        public FORMULA_NAME: string,
        public DUE_DATE: string,
        public OVER_DATE: string,
        public NUM_OF_DAY: string,
        public RATE_CHAGE: string,
        public CHARGE_AMOUNT: string,
        public CHARGE_CCY: string,
        public TRN_AMOUNT: string,
        public TRN_REF_NO: string,
        public TRN_DATE: string,
        public MAKER_ID: string,
        public MAKER_DATE_STAMP: string,
        public CHECKER_ID: string,
        public CHECKER_DATE_STAMP: string, ) {

    }
}
export class Department {
    constructor(
        public ID: string,
        public DEPARTMENT_NAME: string,
        public DEPARTMENT_DESC: string,
        public CREATE_DATE: string,
        public RECORD_STAT: string,
    ) { }
}

export class Branch {
    constructor(
        public BRANCH_CODE: string,
        public BRANCH_NAME: string,
        public BRANCH_ADDRESS: string,
        public TEL_NO: string,
        public MAKER_ID: string,
        public MAKER_DATE_STAMP: string,
        public CHECKER_ID: string,
        public CHECKER_DATE_STAMP: string,
        public PRE_DATE: string,
        public SYSTEM_DATE: string,
        public NEXT_DATE: string,
        public RECORD_STAT: string,
    ) {

    }
}
export class User {
    constructor(public USER_ID: string,
        public USER_NAME: string,
        public USER_PASSWORD: string,
        public BRANCH_CODE: string,
        public BRANCH_NAME: string,
        public GENDER: string,
        public FIRST_NAME: string,
        public LAST_NAME: string,
        public EMAIL: string,
        public TEL_NO: string,
        public DEPT_NAME: string,
        public USER_TYPE: string,
        public AUTO_AUTH: string,
        public CAN_AUTH: string,
        public RECORD_STAT: string,
        public MAKER_ID: string,
        public MAKER_DATE_STAMP: string,
        public CHECKER_ID: string,
        public CHECKER_DATE_STAMP: string,
        public PASSWORD_CHANGE_DATE: string,
        public AUTH_STAT: string, ) { }
}