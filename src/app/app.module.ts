import { CalateralModule } from './modules/calateral/calateral.module';
import { CustomersModule } from './modules/customers/customers.module';

import { DemoMaterialModule } from './shares/module/materialcomponents/materialcomponents.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MainNavComponent } from './shares/components/main-nav/main-nav.component';

import { SystemComponent } from './modules/system/system.component';
import { SystemModule } from './modules/system/system.module';
import { FormsModule } from '@angular/forms';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,

} from '@angular/material';
import { LoginComponent } from './modules/login/login.component';
import { Routes, RouterModule } from '@angular/router';
import { UserControlComponent } from './modules/system/submodules/components/user-control/user-control.component';
import { CustomerComponent } from './modules/customers/customer/customer.component';
import { CalateralComponent } from './modules/calateral/calateral.component';


const appRoute: Routes = [
  { path: 'login', component: LoginComponent },

]
@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,

    SystemComponent,

    LoginComponent,

    CustomerComponent,

    CalateralComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    FlexLayoutModule,
    DemoMaterialModule,
    SystemModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    CustomersModule,
    CalateralModule,
    RouterModule.forRoot(appRoute),

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
